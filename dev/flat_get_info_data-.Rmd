---
title: "flat_get_info_data-.Rmd empty"
output: html_document
editor_options: 
  chunk_output_type: console
---

```{r development, include=FALSE}
library(testthat)
```

```{r development-load}
# Load already included functions if relevant
pkgload::load_all(export_all = FALSE)
```

# get_info_data
    
```{r function-get_info_data}
#' Get info about dataset
#' 
#' a function that gives info about dimensions and names of columns of a dataset
#' 
#' @param data a dataset
#' 
#' @return a list
#' 
#' @export
#' 
get_info_data <- function(data){
  
  if(!is.data.frame(data)){
    stop(data, " is not a dataframe")
  }
  
  #dimensions
  dimensions <- dim(data)
  
  #nom de colonnes
  col_names <- names(data)
  
  #result
  result <- list(dimensions = dimensions, col_names = col_names)
  
  #return
  return(result)
    
}
```
  
```{r example-get_info_data}
get_info_data(data = iris)

```
  
```{r tests-get_info_data}
test_that("get_info_data works", {
  expect_error(
    object = get_info_data(data = "mydata"),
    regexp = "mydata is not a dataframe"
  )
})
```
  

```{r development-inflate, eval=FALSE}
# Run but keep eval=FALSE to avoid infinite loop
# Execute in the console directly
fusen::inflate(flat_file = "dev/flat_get_info_data-.Rmd", vignette_name = "get_info_data")
```

